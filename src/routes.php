<?php


$config=Config::get("tsiou.cms");
print_r($config);

Route::group(['middleware' => 'foo|bar','prefix' => 'manage'], function()
{
    Route::get('/', function()
    {
        // Has Foo And Bar Middleware
    });


    Route::group(['prefix' => 'module/{module}','where' => ['module' => '[A-Za-z]+']], function()
    {
        // Controllers Within The "App\Http\Controllers\Admin\User" Namespace


        Route::get('/', function($module)
        {
            // Has Foo And Bar Middleware
        });


    });


});
?>